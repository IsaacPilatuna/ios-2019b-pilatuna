//
//  SecondViewController.swift
//  HelloWorld
//
//  Created by Isaac on 10/22/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
class SecondViewController: UIViewController {

    @IBOutlet weak var viewTitle: UILabel!
    var customTitle:String?="Second view"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTitle.text = customTitle

    }
    
    
    
    @IBAction func closeView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
