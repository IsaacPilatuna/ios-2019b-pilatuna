import UIKit

var str = "Hello, playground"

var number:Double = 5

number = 5.5

let pi = 3.1415

if (pi>3){
    print ("XD")
}

for x in 1...5 where x%2==0{
    print ("Hello: \(x)")
}

for i in stride (from: 1, to:10, by:2){
    print("Hello: \(i)")
}

var i=0
var j=10

while i<10,j>3{
    print("i: \(i), j: \(j)")
    i += 1
    j -= 1
}

var numbers = [1,2,3,4,5,6,7]
numbers.append(8)
numbers += [9]

var moreNumbers:[Double] = []

let even = numbers
    .filter{ $0 % 2 == 0 }
    .map{ $0 * $0 }

even


let sum = numbers.reduce(0) { (result,next) in
    return result + next
}
sum

let sum2 = numbers.reduce(0) { $0 + $1 }
sum2

let sum3 = numbers.reduce(0, +)
sum3


let dictionary = [
    "name":"Isaac",
    "lastname":"Guerrero",
    "jobTitle":"Student"
]

numbers[1]

dictionary["name"]

let temperature = 16
switch temperature{
case 1...8 where temperature % 2 == 0:
    print("It's cold!")
case 9...12:
    print("Not too cold ")
case 13...17:
    print("Just about right")
default:
    print("---")

}


func myFunction(){
    print("Hey!")
}
myFunction()


func square(number:Int) -> Int{
    return number * number
}

square(number:5)

func sendMail(text:String,to recipient:String){
    print ("Mensaje: \(text). Enviado al correo: \(recipient)")
}

sendMail( text: "Salu2", to: "isapila1996@hotmail.com")

func square2(_ number:Int) -> Int{
    return number * number
}

square2(4)

func sSum(n1: Int, n2: Int, n3: Int = 0) -> Int {
    return n1+n2+n3
}

sSum(n1:1,n2:2)

sSum(n1: 1, n2: 2, n3:3)


func specialSum<T:Numeric>(n1: T, n2: T) -> T {
    return n1 + n2
}

let r1 = specialSum(n1: 5.0, n2: 7.123)
r1

let r2 = specialSum(n1: 4, n2: 4.7)
r2

let coord2D = (x: 4, y: 5)
let coord3D = (x: 4.1, y: 6, z: 8.3)

func badFunction(sideSize: Int) -> (Int, Int){
    return (sideSize * 4, sideSize * sideSize)
}

badFunction(sideSize: 5)
