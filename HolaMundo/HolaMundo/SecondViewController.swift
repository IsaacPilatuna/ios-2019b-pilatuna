//
//  SecondViewController.swift
//  HolaMundo
//
//  Created by Isaac on 10/18/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var viewTitleLabel: UILabel!
    var customTitle:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTitleLabel.text = customTitle

    }
    
    @IBAction func changeTextPressed(_ sender: Any) {
        viewTitleLabel.text="New title"
    }
    
    
    @IBAction func closeView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
