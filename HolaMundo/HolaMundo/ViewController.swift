//
//  ViewController.swift
//  HolaMundo
//
//  Created by Isaac on 10/18/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import SwiftUI

class ViewController: UIViewController {
    
    @IBOutlet weak var helloWorldLabel: UILabel!
    
    @IBOutlet weak var inputTextField: UITextField!
    override func viewDidLoad(){
        super.viewDidLoad()
    }

    
    @IBAction func changeText(_ sender: Any) {
        helloWorldLabel.text=inputTextField.text
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier=="goToSecondViewController"){
            let destinationView=segue.destination as! SecondViewController
            destinationView.customTitle=inputTextField.text
        }
    }
}


