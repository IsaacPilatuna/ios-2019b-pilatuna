//
//  ViewController.swift
//  HelloWorld
//
//  Created by Isaac on 10/22/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var customTitle:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func changeText(_ sender: Any) {
        viewTitleLabel.text=textField.text
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier=="goToSecondViewController"){
            let destinationView=segue.destination as! SecondViewController
            destinationView.customTitle=textField.text
        }
    }
}

