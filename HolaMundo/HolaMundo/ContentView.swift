//
//  ContentView.swift
//  HolaMundo
//
//  Created by Isaac on 10/18/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello World!"/*@END_MENU_TOKEN@*/)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
